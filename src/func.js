const getSum = (str1, str2) => {
  if ( Array.isArray(str1) || Array.isArray(str2) ) {
    return false;
 }

  let num1 = Number(str1);
  let num2 = Number(str2);

  if (typeof num1  != 'number' || typeof num2 != 'number' || isNaN(num1) || isNaN(num2)) {
    return false;
  }

  return String(num1 + num2);
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
   let post = listOfPosts.map(function detour (el) {
      if (el.author == authorName) {
         return [el.post].concat();
      }
   }).filter(function (el) {
      return el != null;
   }); 
 
    let comments = listOfPosts.flatMap(function detour (el) {
         return [el.author == authorName].concat(el.comments?.flatMap(detour) || [])
       }).filter((el) => {
         return el == true;
       });
 
   return `Post:${post.length},comments:${comments.length - post.length}`;
}

const tickets=(people)=> {
  let cash = { 25 : 0, 50 : 0 };

  people.forEach((el) => {
   let coin = Number(el);
   
   switch (coin) {
      case 25:
         cash[coin]++;
         break;
      case 50:
         cash[25]--;
         cash[coin]++;
         break;
      case 100:
         if (cash[50] > 0) {
            cash[50]--;
         } else {
            cash[25]-=2;
         }
         cash[25]--;
         break;
   }
  });
  if (cash[25] < 0) {
    return 'NO';
  }

  return 'YES';
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
